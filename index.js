//Importing modules
const express = require("express");
const mysql = require("mysql");

const app = express();
const PORT = 3000;

// Create a connection to the database
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "appu9492",
  database: "firstdb",
});

// open the MySQL connection
connection.connect((error) => {
  if (error) {
    console.log("A error has been occurred " + "while connecting to database.");
    throw error;
  }
});
//query execution
function query(sql) {
  connection.query(sql, function (error, result) {
    if (error) throw error;
    console.log("Query Executed");
  });
}
//creating a database
function createDB(dbname) {
  var sql = `create database ${dbname}`;
  query(sql);
  console.log("Database  created");
}

app.post("/1", (req, res) => {
  res.send(createDB("databaseone"));
});

//creating a table

function createTable(tablename) {
  var sql =
    "create table " +
    tablename +
    "(name VARCHAR(30), id int(25),college varchar(50), age int);";
  query(sql);
  console.log("table  created");
}

app.post("/2", (req, res) => {
  res.send(createTable("batch"));
});

//creating a record

function createRecord(tablename) {
  var sql =
    "insert into " +
    tablename +
    " values ('harshi',1001,'au',21),('appu',1002,'au',21),('gowthami',1003,'aucew',20),('neelima',1004,'au',21)";
  query(sql);
  console.log("record  created");
}

app.post("/3", (req, res) => {
  res.send(createRecord("batch"));
});

//reading database

function readDB(dbname) {
  var sql = "use " + dbname;
  query(sql);
  console.log("reading database");
}
app.post("/4", (req, res) => {
  res.send(readDB("databaseone"));
});

//reading record

function readRecord(tablename) {
  sql = "select * from " + tablename;
  query(sql);
  console.log("reading record");
}
app.post("/5", (req, res) => {
  res.send(readRecord("batch"));
});

//reading table

function readTable(tablename) {
  sql = "desc " + tablename;
  query(sql);
  console.log("reading table");
}
app.post("/6", (req, res) => {
  res.send(readTable("batch"));
});

//update table
function updateTable(tablename) {
  sql = "ALTER TABLE " + tablename + " ADD COLUMN Quantity int(5)";
  query(sql);
  console.log("update table");
}
app.put("/7", (req, res) => {
  res.send(updateTable("batch"));
});

//update record
function updateRecord(tablename) {
  sql = "update " + tablename + " set rate=30 where quantity>4";

  query(sql);
  console.log("update record");
}
app.put("/8", (req, res) => {
  res.send(updateRecord("batch"));
});

//delete table
function deleteTable(tablename) {
  sql = "drop table "+tablename;

  query(sql);
  console.log("delete table");
}
app.delete("/9", (req, res) => {
  res.send(deleteTable("batch"));
});

//delete record
function deleteRecord(tablename) {
  sql = "delete from "+tablename;
	 query(sql);
  console.log("delete table");
}
app.delete("/10", (req, res) => {
  res.send(deleteRecord("batch"));
});

//delete database
function deleteDatabase(dbname) {
  sql = "drop database"+dbname;
	 query(sql);
  console.log("delete database");
}
app.delete("/11", (req, res) => {
  res.send(deleteDatabase("databaseone"));
});

//If Everything goes correct, Then start Express Server
app.listen(3000, () => {
  console.log(
    "Database connection is Ready and " + "Server is Listening on Port ",
    PORT
  );
});
